#include <iostream>
#include <vector>
#include <map>
using namespace std;

bool containsDuplicate(vector<int> &nums)
{
    map<int, int> freqMap;
    for (int i = 0; i < nums.size(); i++)
    {
        if (freqMap.find(nums[i]) != freqMap.end())
            return true;
        else
            freqMap[nums[i]] = 1;
    }
    return false;
}

int main()
{
    vector<int> vec;
    // vec = {1, 2, 3, 1};
    vec = {1, 2, 3, 4};
    // vec = {1, 1, 1, 3, 3, 4, 3, 2, 4, 2};
    // vec = {1, 2, 1};
    cout << containsDuplicate(vec);
    return 0;
}