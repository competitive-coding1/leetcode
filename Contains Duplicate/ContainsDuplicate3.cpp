#include <iostream>
#include <vector>
#include <map>
using namespace std;

bool containsDuplicate(vector<int> &nums)
{
    int numSize = nums.size();
    for (int i = 0; i < numSize; i++)
    {
        nums[nums[i] % numSize] = nums[nums[i] % numSize] + numSize;
    }
    for (int i = 0; i < numSize; i++)
    {
        if (nums[i] > numSize * 2)
            return true;
    }
    return false;
}

int main()
{
    vector<int> vec;
    // vec = {1, 2, 3, 1};
    // vec = {1, 2, 3, 4};
    // vec = {1, 1, 1, 3, 3, 4, 3, 2, 4, 2};
    // vec = {1, 2, 1};
    // vec = {1, 2};
    vec = {0};
    cout << containsDuplicate(vec);
    return 0;
}