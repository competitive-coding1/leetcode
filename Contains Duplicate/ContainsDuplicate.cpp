#include <iostream>
#include <vector>
using namespace std;

bool containsDuplicate(vector<int> &nums)
{
    for (int i = 0; i < nums.size() - 1; i++)
    {
        for (int j = i + 1; j < nums.size(); j++)
        {
            if (nums[i] == nums[j])
                return true;
        }
    }
    return false;
}

int main()
{
    vector<int> vec;
    // vec = {1, 2, 3, 1};
    // vec = {1, 2, 3, 4};
    // vec = {1, 1, 1, 3, 3, 4, 3, 2, 4, 2};
    vec = {1, 2, 1};
    cout << containsDuplicate(vec);
    return 0;
}