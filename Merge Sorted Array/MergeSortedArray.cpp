#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void merge(vector<int> &nums1, int m, vector<int> &nums2, int n)
{
    int i = 0, j = 0, k = m;
    while (i < m + n && j < n)
    {
        if (n == 0)
            return;
        // cout << "i: " << i << " j: " << j << endl;
        if (nums1[i] <= nums2[j])
        {
            if (nums1[i] == 0 && i >= k)
            {
                nums1[i] = nums2[j];
                j++;
            }
            i++;
        }
        else
        {
            for (int p = k; p > i; p--)
            {
                nums1[p] = nums1[p - 1];
            }
            nums1[i] = nums2[j];
            i++;
            j++;
            k++;
        }
        // printVector(nums1);
    }
}

int main()
{
    vector<int> v1;
    vector<int> v2;
    // v1 = {1, 2, 3, 0, 0, 0};
    // v2 = {2, 5, 6};
    // int l1 = 3;
    // int l2 = 3;
    // v1 = {1};
    // v2 = {};
    // int l1 = 1;
    // int l2 = 0;
    v1 = {0};
    v2 = {1};
    int l1 = 0;
    int l2 = 1;
    // v1 = {2, 5, 0, 0, 0};
    // v2 = {1, 4, 6};
    // int l1 = 2;
    // int l2 = 3;
    // v1 = {4, 5, 6, 0, 0, 0};
    // v2 = {1, 2, 3};
    // int l1 = 3;
    // int l2 = 3;
    // v1 = {-1, 0, 0, 3, 3, 3, 0, 0, 0};
    // v2 = {1, 2, 2};
    // int l1 = 6;
    // int l2 = 3;
    cout << "Before merge v1: ";
    printVector(v1);
    cout << "Before merge v2: ";
    printVector(v2);
    merge(v1, l1, v2, l2);
    cout << "After merge v1: ";
    printVector(v1);
    cout << "After merge v2: ";
    printVector(v2);
}