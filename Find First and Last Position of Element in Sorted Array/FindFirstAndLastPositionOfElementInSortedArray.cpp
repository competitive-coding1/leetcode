#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<int> searchRange(vector<int> &nums, int target)
{
    int start = 0, end = nums.size() - 1, mid, startIndex = -1, endIndex = -1;
    vector<int> res = {-1, -1};

    while (start <= end)
    {
        mid = floor((start + end) / 2);
        if (nums[mid] == target)
        {
            startIndex = mid;
            endIndex = mid;
            break;
        }
        else if (target < nums[mid])
        {
            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }
    if (startIndex != -1)
    {

        while (startIndex >= 0 && nums[startIndex] == target)
            startIndex--;
        while (endIndex < nums.size() && nums[endIndex] == target)
            endIndex++;
        res = {startIndex + 1, endIndex - 1};
    }
    return res;
}

int main()
{
    vector<int> input, output;
    int target;
    // input = {5, 7, 7, 8, 8, 10};
    // target = 8;
    // input = {5, 7, 7, 8, 8, 10};
    // target = 6;
    // input = {5, 7, 7, 8, 8, 10};
    // target = 7;
    // input = {5, 7, 7, 8, 8, 10};
    // target = 10;
    // input = {5, 7, 7, 8, 8, 10};
    // target = 5;
    // input = {};
    // target = 0;
    input = {1};
    target = 1;
    // input = {2, 2};
    // target = 2;
    output = searchRange(input, target);
    printVector(output);
    return 0;
}