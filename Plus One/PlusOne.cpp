#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<int> plusOne(vector<int> &digits)
{
    int n = digits.size();
    int last_digit_sum = digits[n - 1] + 1;
    vector<int> result(n + 1, 0);
    result[n] = last_digit_sum % 10;
    int carry = last_digit_sum / 10;

    for (int i = n - 2; i >= 0; i--)
    {
        result[i + 1] = (digits[i] + carry) % 10;
        carry = (digits[i] + carry) / 10;
    }

    if (carry != 0)
        result[0] = carry;
    else
        result.erase(result.begin());
    return result;
}

int main()
{
    vector<int> input, output;
    // input = {1, 2, 3};
    input = {4, 3, 2, 1};
    // input = {9, 9, 9, 9};
    output = plusOne(input);
    printVector(output);
    return 0;
}