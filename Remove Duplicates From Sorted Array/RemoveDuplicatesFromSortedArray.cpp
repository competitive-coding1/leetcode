#include <iostream>
#include <vector>
#include <limits>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

int removeDuplicates(vector<int> &nums)
{
    int i = 0, j, k;
    while (i < nums.size() && nums[i] != INT16_MAX)
    {
        j = k = i + 1;
        while (j < nums.size() && (nums[j] == nums[i]))
        {
            j++;
        }
        while (j < nums.size() && nums[j] != INT16_MAX)
        {
            nums[k] = nums[j];
            j++;
            k++;
        }
        while (k < nums.size() && nums[k] != INT16_MAX)
        {
            nums[k] = INT16_MAX;
            k++;
        }
        i++;
    }
    return i;
}

int main()
{
    // vector<int> v1 = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
    // vector<int> v1 = {1, 1, 2};
    // vector<int> v1 = {1, 1};
    vector<int> v1 = {1};
    // vector<int> v1 = {};
    cout << "Size of vector is: " << v1.size() << endl;
    cout << "Vector at the start: ";
    printVector(v1);
    cout << "Unique numbers in list: " << removeDuplicates(v1) << endl;
    cout << "Vector at the end: ";
    printVector(v1);
    return 0;
}