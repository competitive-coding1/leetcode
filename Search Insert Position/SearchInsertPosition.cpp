#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int searchInsert(vector<int> &nums, int target)
{
    if (target > nums.at(nums.size() - 1))
    {
        return nums.size();
    }
    else if (!(target < nums.at(0)))
    {
        int l = 0, r = nums.size() - 1, m;

        while (l <= r)
        {
            m = floor((l + r) / 2);
            if (nums.at(m) == target)
            {
                return m;
            }
            else if (target < nums.at(m))
            {
                r = m - 1;
            }
            else
            {
                l = m + 1;
            }
        }
        return l;
    }
    return 0;
}

int main()
{
    vector<int> v1;
    v1.push_back(1);
    v1.push_back(3);
    v1.push_back(5);
    v1.push_back(6);
    cout << searchInsert(v1, 0);

    return 0;
}