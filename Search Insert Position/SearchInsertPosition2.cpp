#include <iostream>
#include <vector>
#include <bits/stdc++.h>
using namespace std;

int searchInsert(vector<int> &nums, int target)
{
    vector<int>::iterator it;
    it = find(nums.begin(), nums.end(), target);
    if (it != nums.end())
    {
        return it - nums.begin();
    }
    else if (target > nums.at(nums.size() - 1))
    {
        return nums.size();
    }
    else if (!(target < nums.at(0)))
    {
        int j = nums.size() - 1;
        for(; j > 0; j--)
        {
            if(nums.at(j - 1) < target && target < nums.at(j))
            {
                return j;
            }
        }
    }
    return 0;
}

int main()
{
    vector<int> v1;
    v1.push_back(1);
    v1.push_back(3);
    v1.push_back(5);
    v1.push_back(6);
    cout << searchInsert(v1, 2);

    return 0;
}