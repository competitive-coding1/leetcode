#include <iostream>
#include <limits>
using namespace std;

int reverse(int x)
{
    long reverseNumber = 0;

    while (x)
    {
        reverseNumber = reverseNumber * 10 + (x % 10);
        if (reverseNumber > INT_MAX || reverseNumber < INT_MIN)
            return 0;
        cout << reverseNumber << endl;
        x = x / 10;
    }
    return reverseNumber;
}

int main()
{
    long int num = 1534236469;
    cout << "Reverse number of " << num << " is: " << reverse(num);
}