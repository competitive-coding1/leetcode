#include <iostream>
#include <cmath>
using namespace std;

int search(int arr[], int x, int size)
{
    int l = 0, r = size - 1, m;
    while (l <= r)
    {
        m = floor((l + r) / 2);
        if (arr[m] == x)
        {
            return m;
        }
        else if (x < arr[m])
        {
            l = l;
            r = m - 1;
        }
        else
        {
            r = r;
            l = m + 1;
        }
    }
    return -1;
}

int main()
{
    // int elements[] = {1, 4, 9, 12, 23, 45, 67, 89};
    int elements[] = {10, 11, 16, 20};
    int elementsSize = sizeof(elements) / sizeof(elements[0]);
    // int elementToFind = 20;
    int elementToFind = 13;
    int elementPosition = search(elements, elementToFind, elementsSize);
    cout << "Position of " << elementToFind << " in array is: " << elementPosition << endl;
    cout << "\n";
    return 0;
}