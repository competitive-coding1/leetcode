#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]";
}

void printVectorOfVector(vector<vector<int>> &vec)
{
    cout << "[";
    for (int i = 0; i < vec.size(); i++)
    {
        printVector(vec[i]);
        if (i < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<vector<int>> matrixReshape(vector<vector<int>> &mat, int r, int c)
{

    int m = mat.size();
    int n = mat[0].size();
    if (m * n != r * c)
        return mat;
    vector<vector<int>> reshapedMat(r, vector<int>(c, 0));
    for (int i = 0; i < r * c; i++)
    {
        reshapedMat[i / c][i % c] = mat[i / n][i % n];
    }
    return reshapedMat;
}

int main()
{
    vector<vector<int>> myMet;
    // myMet = {{1, 2}, {3, 4}};
    myMet = {{2, 3, 7, 6}, {9, 6, 3, 3}, {2, 4, 8, 8}, {7, 6, 5, 5}};
    printVectorOfVector(myMet);
    // myMet = matrixReshape(myMet, 1, 4);
    // myMet = matrixReshape(myMet, 2, 4);
    myMet = matrixReshape(myMet, 8, 2);
    // myMet = matrixReshape(myMet, 2, 8);
    printVectorOfVector(myMet);
    return 0;
}