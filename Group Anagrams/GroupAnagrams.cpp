#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <bits/stdc++.h>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printVectorString(vector<string> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<vector<string>> groupAnagrams(vector<string> &strs)
{
    map<string, vector<string>> mp;
    vector<vector<string>> result;

    for (auto str : strs)
    {
        string sorted_str = str;
        sort(sorted_str.begin(), sorted_str.end());
        mp[sorted_str].push_back(str);
    }

    for (auto i : mp)
        result.push_back(i.second);

    return result;
}

int main()
{
    vector<string> v = {"eat", "tea", "tan", "ate", "nat", "bat"};
    vector<vector<string>> v1 = groupAnagrams(v);
    for (auto i : v1)
        printVectorString(i);
    return 0;
}