#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;

void printVector(vector<string> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<string> letterCombinations(string digits)
{
    map<char, vector<string>> numberMap = {
        {'1', {""}},
        {'2', {"a", "b", "c"}},
        {'3', {"d", "e", "f"}},
        {'4', {"g", "h", "i"}},
        {'5', {"j", "k", "l"}},
        {'6', {"m", "n", "o"}},
        {'7', {"p", "q", "r", "s"}},
        {'8', {"t", "u", "v"}},
        {'9', {"w", "x", "y", "z"}}};

    vector<string> output;
    vector<string> temp;

    if (digits.length() == 1 || digits.length() == 0)
    {
        return numberMap[digits[0]];
    }
    for (int i = 0; i < digits.length() - 1; i++)
    {
        if (output.empty())
            temp = numberMap[digits[i]];
        else
            temp = output;
        int limit = temp.size();
        output.clear();
        for (int j = 0; j < temp.size(); j++)
        {
            for (int k = 0; k < numberMap[digits[i + 1]].size(); k++)
            {
                output.push_back(temp[j] + numberMap[digits[i + 1]][k]);
            }
        }
    }
    return output;
}

int main()
{
    cout << "Letter combination: " << endl;
    vector<string> answer = letterCombinations("");
    printVector(answer);
    return 0;
}