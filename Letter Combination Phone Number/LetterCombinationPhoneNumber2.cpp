#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;

void printVector(vector<string> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

vector<string> letterCombinations(string digits)
{
    map<char, vector<string>> numberMap = {
        {'1', {""}},
        {'2', {"a", "b", "c"}},
        {'3', {"d", "e", "f"}},
        {'4', {"g", "h", "i"}},
        {'5', {"j", "k", "l"}},
        {'6', {"m", "n", "o"}},
        {'7', {"p", "q", "r", "s"}},
        {'8', {"t", "u", "v"}},
        {'9', {"w", "x", "y", "z"}}};

    vector<string> output, temp, temp2;

    if (digits.length() == 1 || digits.length() == 0)
    {
        return numberMap[digits[0]];
    }
    temp2 = numberMap[digits.back()];
    digits.pop_back();
    temp = letterCombinations(digits);
    for (int j = 0; j < temp.size(); j++)
    {
        for (int k = 0; k < temp2.size(); k++)
        {
            output.push_back(temp[j] + temp2[k]);
        }
    }
    return output;
}

int main()
{
    cout << "Letter combination: " << endl;
    vector<string> answer = letterCombinations("5984");
    printVector(answer);
    return 0;
}