#include <iostream>
#include <string>
#include <map>
using namespace std;

void printMap(map<char, int> &map)
{
    for (auto entry : map)
    {
        cout << entry.first << ": " << entry.second << " ";
    }
}

bool canConstruct(string ransomNote, string magazine)
{
    map<char, int> map1, map2;
    for (int i = 0; i < ransomNote.length(); i++)
    {
        if (map1.count(ransomNote[i]))
        {
            map1[ransomNote[i]]++;
        }
        else
        {
            map1[ransomNote[i]] = 1;
        }
    }
    printMap(map1);
    cout << endl;

    for (int i = 0; i < magazine.length(); i++)
    {
        if (map2.count(magazine[i]))
        {
            map2[magazine[i]]++;
        }
        else
        {
            map2[magazine[i]] = 1;
        }
    }
    printMap(map2);
    cout << endl;

    for (auto entry : map1)
    {
        if (!map2.count(entry.first) || !(entry.second <= map2[entry.first]))
        {
            return false;
        }
    }
    return true;
}

int main()
{
    string a, b;
    a = "a";
    b = "b";
    // a = "aa";
    // b = "ab";
    // a = "aa";
    // b = "aab";
    // a = "bg";
    // b = "efjbdfbdgfjhhaiigfhbaejahgfbbgbjagbddfgdiaigdadhcfcj";
    // a = "fihjjjjei";
    // b = "hjibagacbhadfaefdjaeaebgi";
    cout << canConstruct(a, b) << endl;
    return 0;
}