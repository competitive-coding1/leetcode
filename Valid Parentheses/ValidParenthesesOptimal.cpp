#include <iostream>
#include <stack>
using namespace std;

bool isValid(string s)
{
    stack<char> stack;

    for (auto i : s)
    {
        if (i == '(' || i == '{' || i == '[')
        {
            stack.push(i);
        }
        else
        {
            if (stack.empty() || (i == ')' && stack.top() != '(') || (i == '}' && stack.top() != '{') || (i == ']' && stack.top() != '['))
                return false;

            stack.pop();
        }
    }
    return stack.empty();
}

int main()
{
    cout << isValid("}");
    return 0;
}