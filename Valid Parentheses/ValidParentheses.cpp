#include <iostream>
using namespace std;

bool isValid(string s)
{
    string c = "({[)}]";

    while (s.length() != 0)
    {
        if (s.length() == 1 || c.find(s[0]) > 2)
            return false;

        int i = 0;
        while (c.find(s[i]) < 3)
        {
            i++;
        }
        if ((c.find(s[i]) - c.find(s[i - 1])) != 3)
            return false;
        s.erase(i - 1, 2);
    }
    return true;
}

int main()
{
    cout << isValid("){") << endl;
    return 0;
}