#include <iostream>
#include <vector>
#include <string>
#include <map>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printMap(map<char, vector<int>> &map)
{
    for (auto entry : map)
    {
        cout << entry.first << ": ";
        printVector(entry.second);
    }
}

int firstUniqChar(string s)
{
    map<char, vector<int>> charMap;

    for (int i = 0; i < s.length(); i++)
    {
        charMap[s[i]].push_back(i);
    }
    // printMap(charMap);
    int minIndex = s.length();
    for (auto entry : charMap)
    {
        if (entry.second.size() == 1)
        {
            if (entry.second[0] < minIndex)
                minIndex = entry.second[0];
        }
    }
    if (minIndex != s.length())
        return minIndex;
    return -1;
}

int main()
{
    string str;
    // str = "leetcode";
    // str = "loveleetcode";
    str = "aabb";
    cout << firstUniqChar(str) << endl;
    return 0;
}