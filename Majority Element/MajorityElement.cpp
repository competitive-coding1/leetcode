#include <iostream>
#include <vector>
#include <map>
using namespace std;

int majorityElement(vector<int> &nums)
{
    map<int, int> freqMap;
    int maxCount = 0;
    int maxIndex = 0;

    for (int i = 0; i < nums.size(); i++)
    {
        if (freqMap.count(nums[i]))
        {
            freqMap[nums[i]]++;
        }
        else
        {
            freqMap[nums[i]] = 1;
        }
        if (freqMap[nums[i]] > maxCount)
        {
            maxCount = freqMap[nums[i]];
            maxIndex = i;
        }
    }
    return nums[maxIndex];
}

int main()
{
    vector<int> input;
    int output;
    // input = {3, 2, 3};
    // input = {2, 2, 1, 1, 1, 2, 2};
    input = {6, 5, 5};
    output = majorityElement(input);
    cout << output << endl;
    return 0;
}