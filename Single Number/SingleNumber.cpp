#include <iostream>
#include <vector>
#include <bits/stdc++.h>
using namespace std;

int singleNumber(vector<int> &nums)
{
    sort(nums.begin(), nums.end());
    int i = 0;
    while (i < nums.size() - 1)
    {
        if (nums[i] != nums[i + 1])
            return nums[i];
        else
        {
            while (nums[i] == nums[i + 1])
            {
                i++;
            }
            i++;
        }
    }
    return nums[nums.size() - 1];
}

int main()
{
    vector<int> input;
    int output;
    // input = {2, 2, 1};
    // input = {1};
    // input = {4, 4, 5};
    // input = {4, 1, 2, 1, 2};
    input = {2, 2, 3, 2};
    output = singleNumber(input);
    cout << "Output: " << output << endl;
    return 0;
}