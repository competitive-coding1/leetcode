/* 
This solution uses XOR operation

Truth table
a  b  a^b(a XOR b)
0  0   0   
0  1   1
1  0   1
1  1   0

So if two numbers are same in the array. They will add up to be 0 and only ouptput will be distinct number.;

*/

#include <iostream>
#include <vector>
using namespace std;

int singleNumber(vector<int> &nums)
{
    int result = 0;
    for (int i = 0; i < nums.size(); i++)
    {
        result ^= nums[i];
    }
    return result;
}

int main()
{
    vector<int> input;
    int output;
    // input = {2, 2, 1};
    // input = {1};
    // input = {4, 4, 5};
    input = {4, 1, 2, 1, 2};
    output = singleNumber(input);
    cout << "Output: " << output << endl;
    return 0;
}