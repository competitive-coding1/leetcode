#include <iostream>
#include <vector>
#include <map>
#include <bits/stdc++.h>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printMap(map<int, int> &m)
{
    cout << "-----Start-----" << endl;
    for (auto val : m)
    {
        cout << val.first << " : " << val.second << endl;
    }
    cout << "-----End-----" << endl;
}

vector<int> intersect(vector<int> &nums1, vector<int> &nums2)
{
    set<int> s1(nums1.begin(), nums1.end());
    set<int> s2(nums2.begin(), nums2.end());
    set<int> intersection;
    set_intersection(s1.begin(), s1.end(), s2.begin(), s2.end(), inserter(intersection, intersection.begin()));
    vector<int> result(intersection.begin(), intersection.end());
    return result;
}

int main()
{
    vector<int> v1;
    vector<int> v2;
    vector<int> answer;
    v1 = {1, 2, 2, 1};
    v2 = {2, 2};
    // v1 = {4, 9, 5};
    // v2 = {9, 4, 9, 8, 4};
    answer = intersect(v1, v2);
    printVector(answer);
    return 0;
}