#include <iostream>
#include <vector>
#include <map>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printMap(map<int, int> &m)
{
    cout << "-----Start-----" << endl;
    for (auto val : m)
    {
        cout << val.first << " : " << val.second << endl;
    }
    cout << "-----End-----" << endl;
}

vector<int> intersect(vector<int> &nums1, vector<int> &nums2)
{
    map<int, int> m1;
    map<int, int> m2;
    vector<int> result;

    for (int i = 0; i < nums1.size(); i++)
    {
        if (m1.find(nums1[i]) != m1.end())
        {
            m1[nums1[i]]++;
        }
        else
        {
            m1[nums1[i]] = 1;
        }
    }
    // printMap(m1);
    for (int i = 0; i < nums2.size(); i++)
    {
        if (m2.find(nums2[i]) != m2.end())
        {
            m2[nums2[i]]++;
        }
        else
        {
            m2[nums2[i]] = 1;
        }
    }
    // printMap(m2);

    for (auto val : m1)
    {
        if (m2.find(val.first) != m2.end())
        {
            int freq = min(m1[val.first], m2[val.first]);
            for (int i = 0; i < freq; i++)
                result.push_back(val.first);
        }
    }
    return result;
}

int main()
{
    vector<int> v1;
    vector<int> v2;
    vector<int> answer;
    // v1 = {1, 2, 2, 1};
    // v2 = {2, 2};
    v1 = {4, 9, 5};
    v2 = {9, 4, 9, 8, 4};
    answer = intersect(v1, v2);
    printVector(answer);
    return 0;
}