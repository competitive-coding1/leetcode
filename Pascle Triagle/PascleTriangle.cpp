#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printVectorOfVector(vector<vector<int>> &vec)
{
    for (auto v : vec)
    {
        printVector(v);
    }
}

vector<vector<int>> generate(int numRows)
{
    vector<vector<int>> triangle = {{}};
    if (numRows == 0)
        return triangle;
    triangle = {{1}};
    if (numRows == 1)
    {
        return triangle;
    }
    else
    {
        for (int i = 1; i < numRows; i++)
        {
            triangle.push_back(vector<int>(i + 1, 0));
            for (int j = 0; j < triangle[i].size(); j++)
            {
                if (j == 0)
                {
                    triangle[i][j] = triangle[i - 1][j];
                }
                else if (j == triangle[i - 1].size())
                {
                    triangle[i][j] = triangle[i - 1][j - 1];
                }
                else
                {
                    triangle[i][j] = triangle[i - 1][j - 1] + triangle[i - 1][j];
                }
            }
        }
    }
    return triangle;
}

int main()
{
    vector<vector<int>> vec;
    vec = generate(0);
    // vec = generate(1);
    // vec = generate(2);
    // vec = generate(3);
    // vec = generate(4);
    // vec = generate(5);
    printVectorOfVector(vec);
    return 0;
}