#include <iostream>
#include <vector>
#include <map>
#include <string>
using namespace std;

void printVector(vector<char> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printVectorOfVector(vector<vector<char>> &vec)
{
    for (auto v : vec)
    {
        printVector(v);
    }
}

bool isValidSudoku(vector<vector<char>> &board)
{
    map<char, int> numMap1, numMap2;
    for (int i = 0; i < 9; i++)
    {
        for (int j = 0; j < 9; j++)
        {
            if (board[i][j] == '.')
            {
                // Do nothing
            }
            else if (!numMap1.count(board[i][j]))
                numMap1[board[i][j]] = 1;
            else
                return false;

            if (board[j][i] == '.')
            {
                // Do nothing
            }
            else if (!numMap2.count(board[j][i]))
                numMap2[board[j][i]] = 1;
            else
                return false;
        }
        numMap1.clear();
        numMap2.clear();
    }

    for (int i = 0; i <= 8; i += 3)
    {
        for (int j = 0; j <= 8; j += 3)
        {
            for (int p = 0; p <= 2; p++)
            {
                if (board[i + p][j + p] == '.')
                {
                    // Do nothing
                }
                else if (!numMap1.count(board[i + p][j + p]))
                    numMap1[board[i + p][j + p]] = 1;
                else
                    return false;

                if (board[i + p][j + 2 - p] == '.')
                {
                    // Do nothing
                }
                else if (!numMap2.count(board[i + p][j + 2 - p]))
                    numMap2[board[i + p][j + 2 - p]] = 1;
                else
                    return false;
            }
            numMap1.clear();
            numMap2.clear();
        }
    }

    for (int i = 0; i <= 8; i += 3)
    {
        for (int j = 0; j <= 8; j += 3)
        {
            for (int p = 0; p <= 2; p++)
            {
                for (int q = 0; q <= 2; q++)
                {
                    if (board[i + p][j + q] == '.')
                    {
                        // Do nothing
                    }
                    else if (!numMap1.count(board[i + p][j + q]))
                        numMap1[board[i + p][j + q]] = 1;
                    else
                        return false;

                    if (board[i + p][j + 2 - q] == '.')
                    {
                        // Do nothing
                    }
                    else if (!numMap2.count(board[i + p][j + 2 - q]))
                        numMap2[board[i + p][j + 2 - q]] = 1;
                    else
                        return false;
                }
            }
            numMap1.clear();
            numMap2.clear();
        }
    }

    return true;
}

int main()
{
    vector<vector<char>> sudokuBorad;
    sudokuBorad = {
        {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
        {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
        {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
        {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
        {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
        {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
        {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
        {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
        {'.', '.', '.', '.', '8', '.', '.', '7', '9'}};
    printVectorOfVector(sudokuBorad);
    cout << isValidSudoku(sudokuBorad);
}