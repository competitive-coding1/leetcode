#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

int removeElement(vector<int> &nums, int val)
{
    int i = 0, n = nums.size();
    while (i < n)
    {
        if (nums[i] == val)
        {
            nums[i] = nums[n - 1];
            n--;
        }
        else
        {
            i++;
        }
    }
    return n;
}

int main()
{
    vector<int> v1 = {3, 2, 2, 3};
    // vector<int> v1 = {0, 1, 2, 2, 3, 0, 4, 2};
    // vector<int> v1 = {1};
    // vector<int> v1 = {1, 2};
    // vector<int> v1 = {2};
    // vector<int> v1 = {3, 3, 3};
    // vector<int> v1 = {3, 3};
    printVector(v1);
    int k = removeElement(v1, 3);
    cout << "Remaining elements: " << k << endl;
    printVector(v1);
    return 0;
}