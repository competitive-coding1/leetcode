#include <iostream>
#include <map>
#include <string>
using namespace std;

int romanToInt(string s)
{
    map<string, int> romanMap = {
        {"I", 1}, {"V", 5}, {"X", 10}, {"L", 50}, {"C", 100}, {"D", 500}, {"M", 1000}, {"IV", 4}, {"IX", 9}, {"XL", 40}, {"XC", 90}, {"CD", 400}, {"CM", 900}};
    int sum = 0;
    int i = 0;
    string combo = "";

    while (i < s.length())
    {
        combo = string(1, s[i]) + s[i + 1];
        if (romanMap.find(combo) != romanMap.end())
        {
            sum += romanMap[combo];
            i += 2;
        }
        else
        {
            sum += romanMap[string(1, s[i])];
            i++;
        }
        combo = "";
    }
    return sum;
}

int main()
{
    string roman = "III";
    // string roman = "LVIII";
    // string roman = "MCMXCIV";
    cout << "Roman " << roman << " to int is: " << romanToInt(roman) << endl;
    return 0;
}