#include <iostream>
#include <map>
#include <string>
using namespace std;

int romanToInt(string s)
{
    map<char, int> romanMap = {
        {'I', 1}, {'V', 5}, {'X', 10}, {'L', 50}, {'C', 100}, {'D', 500}, {'M', 1000}};
    int sum = 0;
    int i = 0;

    while (i < s.length())
    {
        if (s[i] == 'I')
        {
            if (s[i + 1] == 'V' || s[i + 1] == 'X')
            {
                sum += romanMap[s[i + 1]] - romanMap[s[i]];
                i += 2;
            }
            else
            {
                sum += romanMap[s[i]];
                i += 1;
            }
        }
        else if (s[i] == 'X')
        {
            if (s[i + 1] == 'L' || s[i + 1] == 'C')
            {
                sum += romanMap[s[i + 1]] - romanMap[s[i]];
                i += 2;
            }
            else
            {
                sum += romanMap[s[i]];
                i += 1;
            }
        }
        else if (s[i] == 'C')
        {
            if (s[i + 1] == 'D' || s[i + 1] == 'M')
            {
                sum += romanMap[s[i + 1]] - romanMap[s[i]];
                i += 2;
            }
            else
            {
                sum += romanMap[s[i]];
                i += 1;
            }
        }
        else
        {
            sum += romanMap[s[i]];
            i += 1;
        }
    }
    return sum;
}

int main()
{
    // string roman = "III";
    // string roman = "LVIII";
    string roman = "MCMXCIV";
    cout << "Roman " << roman << " to int is: " << romanToInt(roman) << endl;
    return 0;
}