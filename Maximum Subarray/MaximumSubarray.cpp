#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

int maxSubarray(vector<int> &nums)
{
    int gloabalMax = INT32_MIN;
    int localMax = 0;
    vector<int> indices = {0, 0};

    for (int i = 0; i < nums.size(); i++)
    {
        localMax = 0;
        for (int j = i; j < nums.size(); j++)
        {
            localMax += nums[j];

            if (localMax > gloabalMax)
            {
                gloabalMax = localMax;
                indices[0] = i;
                indices[1] = j;
            }
        }
    }
    printVector(indices);
    return gloabalMax;
}

int main()
{
    vector<int> v1;
    // v1 = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    // v1 = {1};
    v1 = {5, 4, -1, 7, 8};
    int maxSum = maxSubarray(v1);
    cout << "Maximum sum is: " << maxSum << endl;
    return 0;
}