// O(n) time complexity solution

#include <iostream>
#include <vector>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

int maxSubarray(vector<int> &nums)
{
    int currentSum = 0;
    // Keeping maxSum as -Inf to overcome the case when all the array elements are nagative
    int maxSum = INT32_MIN;
    // vector<int> indices = {0, 0};

    for (int i = 0; i < nums.size(); i++)
    {
        currentSum += nums[i];

        if (currentSum > maxSum)
        {
            maxSum = currentSum;
            // indices[1] = i;
        }

        // Condition while all the elements in array are nagative
        if (currentSum + maxSum < maxSum)
        {
            currentSum = 0;
            // indices[0] = i + 1;
        }
    }
    // printVector(indices);
    return maxSum;
}

int main()
{
    vector<int> v1;
    // v1 = {-2, 1, -3, 4, -1, 2, 1, -5, 4};
    // v1 = {1};
    // v1 = {5, 4, -1, 7, 8};
    v1 = {-1};
    // v1 = {-2, -1};
    // v1 = {-2, -1, -6, -4, 5};
    int maxSum = maxSubarray(v1);
    cout << "Maximum sum is: " << maxSum << endl;
    return 0;
}