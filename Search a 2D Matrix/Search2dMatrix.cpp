#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]";
}

void printVectorOfVector(vector<vector<int>> &vec)
{
    cout << "[";
    for (int i = 0; i < vec.size(); i++)
    {
        printVector(vec[i]);
        if (i < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

bool binarySearch(vector<int> &nums, int target)
{
    int start = 0;
    int end = nums.size() - 1;
    int mid;

    while (start <= end)
    {
        mid = floor((start + end) / 2);
        if (nums[mid] == target)
            return true;
        else if (target < nums[mid])
        {
            end = mid - 1;
        }
        else
        {
            start = mid + 1;
        }
    }
    return false;
}

bool searchMatrix(vector<vector<int>> &matrix, int target)
{
    for (int i = 0; i < matrix.size(); i++)
    {
        if (target >= matrix[i][0] && target <= matrix[i][matrix[i].size() - 1])
        {
            return binarySearch(matrix[i], target);
        }
    }
    return false;
}

int main()
{
    vector<vector<int>> data;
    int target;
    // data = {{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}};
    // target = 3;
    data = {{1, 3, 5, 7}, {10, 11, 16, 20}, {23, 30, 34, 60}};
    target = 13;
    cout << searchMatrix(data, target) << endl;
    return 0;
}