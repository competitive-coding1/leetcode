#include <iostream>
#include <regex>
#include <string>
using namespace std;

bool isMatch(string s, string p)
{
    return regex_match(s, regex(p));
}

int main()
{
    string str, reg;
    str = "ab";
    reg = ".*";
    cout << isMatch(str, reg);

    return 0;
}