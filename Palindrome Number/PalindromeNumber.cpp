#include <iostream>
#include <vector>
using namespace std;

bool IsPalindrome(int x)
{
    vector<int> v1;

    if (x < 0)
        return false;

    while (x)
    {
        v1.push_back(x % 10);
        x = x / 10;
    }
    int j = 0;
    int numberLen = v1.size();
    for (j = 0; j < numberLen / 2; j++)
    {
        if (v1.at(j) != v1.at(numberLen - 1 - j))
            break;
    }
    if (j == numberLen / 2)
        return true;
    else
        return false;
}

int main()
{

    cout << IsPalindrome(010) << endl;
    return 0;
}