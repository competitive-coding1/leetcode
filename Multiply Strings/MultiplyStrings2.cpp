#include <iostream>
#include <vector>
#include <string>
using namespace std;

void printVector(vector<int> &vec)
{
    cout << "[";
    for (int p = 0; p < vec.size(); p++)
    {
        cout << vec[p];
        if (p < vec.size() - 1)
            cout << ", ";
    }
    cout << "]" << endl;
}

void printVectorOfVector(vector<vector<int>> &vec)
{
    for (auto v : vec)
    {
        printVector(v);
    }
}

string multiply(string num1, string num2)
{
    if (num1 == "0" || num2 == "0")
        return "0";
    int l1 = num1.length();
    int l2 = num2.length();
    vector<vector<int>> result(l2, vector<int>(l1 + l2, 0));

    for (int i = 0; i < l2; i++)
    {
        int insertIndex = result[i].size() - l2 + i;
        int carry = 0, j;
        for (j = l1 - 1; j >= 0; j--)
        {
            int mul = (num2[i] - '0') * (num1[j] - '0') + carry;
            result[i][insertIndex] = mul % 10;
            insertIndex--;
            carry = mul / 10;
        }
        if (j < 0 && carry != 0)
            result[i][insertIndex] = carry;
    }
    printVectorOfVector(result);
    vector<int> total_sum;
    unsigned long long sum = 0;
    int carry = 0;
    for (int i = l1 + l2 - 1; i >= 0; i--)
    {
        sum = 0;
        for (int j = 0; j < result.size(); j++)
        {
            sum += result[j][i];
        }
        sum += carry;
        total_sum.push_back(sum % 10);
        carry = sum / 10;
    }
    printVector(total_sum);
    string output;
    for (int i = total_sum.size() - 1; i >= 0; i--)
    {
        // output += to_string(total_sum[i]);
        output += total_sum[i] + '0';
    }
    int i = 0;
    while (output[i] == '0')
        i++;
    output.erase(0, i);
    return output;
}

int main()
{
    // string answer = multiply("123", "456");
    // string answer = multiply("9", "9");
    // string answer = multiply("12", "9");
    // string answer = multiply("123456789", "987654321");
    // string answer = multiply("408", "5");
    // string answer = multiply("2", "3");
    string answer = multiply("0", "0");
    cout << "Result: " << answer;
    return 0;
}