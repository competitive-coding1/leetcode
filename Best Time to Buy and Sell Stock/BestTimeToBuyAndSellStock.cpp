#include <iostream>
#include <vector>
using namespace std;

int maxProfit(vector<int> &prices)
{
    int maximumProfit = 0;
    int curProfit = 0;

    for (int i = 0; i < prices.size() - 1; i++)
    {
        curProfit = 0;
        for (int j = i + 1; j < prices.size(); j++)
        {
            curProfit = prices[j] - prices[i];
            if (curProfit > maximumProfit)
                maximumProfit = curProfit;
        }
    }
    return maximumProfit;
}

int main()
{
    vector<int> v1;
    // v1 = {7, 1, 5, 3, 6, 4};
    v1 = {7, 6, 4, 3, 1};
    int maxProfitGain = maxProfit(v1);
    cout << "Max profit is: " << maxProfitGain << endl;
    return 0;
}