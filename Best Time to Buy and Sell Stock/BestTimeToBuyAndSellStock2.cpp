#include <iostream>
#include <vector>
using namespace std;

int maxProfit(vector<int> &prices)
{
    int maximumProfit = 0;
    int curProfit = 0;
    int buyDay = 0;

    for (int i = 1; i < prices.size(); i++)
    {
        curProfit = prices[i] - prices[buyDay];
        // cout << "i: " << i << " buyDay: " << buyDay << " curProfit: " << curProfit << endl;
        if (curProfit > maximumProfit)
            maximumProfit = curProfit;

        if (prices[buyDay] > prices[i])
            buyDay = i;
    }
    return maximumProfit;
}

int main()
{
    vector<int> v1;
    v1 = {7, 1, 5, 3, 6, 4};
    // v1 = {7, 6, 4, 3, 1};
    // v1 = {1, 2};
    int maxProfitGain = maxProfit(v1);
    cout << "Max profit is: " << maxProfitGain << endl;
    return 0;
}