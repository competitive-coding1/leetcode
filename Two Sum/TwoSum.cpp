#include <iostream>
#include <vector>
#include <map>
using namespace std;

vector<int> twoSum(vector<int> &nums, int target)
{
    vector<int> indices;
    map<int, int> indicesMap;

    for (int i = 0; i < nums.size(); i++)
    {
        if (indicesMap.find(target - nums[i]) != indicesMap.end())
        {
            indices.push_back(i);
            indices.push_back(indicesMap[target - nums[i]]);
            break;
        }
        else
        {
            indicesMap.insert(pair<int, int>(nums[i], i));
        }
    }
    return indices;
}

int main()
{
    vector<int> v1;
    v1.push_back(2);
    v1.push_back(7);
    v1.push_back(11);
    v1.push_back(15);
    int target = 9;
    vector<int> result;
    result = twoSum(v1, target);
    cout << "Size of result: " << result.size() << endl;
    for (int i = 0; i < result.size(); i++)
    {
        cout << "result[" << i << "] = " << result.at(i) << endl;
    }
    return 0;
}