#include <iostream>
#include <string>
using namespace std;

int strStr(string haystack, string needle)
{
    int m = haystack.length();
    int n = needle.length();

    if (n == 0)
        return 0;
    for (int i = 0; i <= m - n; i++)
    {
        int j = 0;
        for (; j < n; j++)
        {
            if (haystack[i + j] != needle[j])
                break;
        }
        if (j == n)
            return i;
    }
    return -1;
}

int main()
{
    cout << strStr("kevin", "n");
    return 0;
}