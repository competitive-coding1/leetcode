#include <iostream>
#include <string>
#include <bits/stdc++.h>
using namespace std;

bool isAnagram(string s, string t)
{
    if (s.length() != t.length())
        return false;

    sort(s.begin(), s.end());
    sort(t.begin(), t.end());

    if (s == t)
        return true;

    return false;
}

int main()
{
    string s1, s2;
    // s1 = "rat";
    // s2 = "tar";
    // s1 = "rat";
    // s2 = "car";
    s1 = "anagram";
    s2 = "nagaram";
    cout << isAnagram(s1, s2) << endl;
    return 0;
}