#include <iostream>
#include <string>
#include <bits/stdc++.h>
using namespace std;

void printMap(map<char, int> &map)
{
    for (auto entry : map)
    {
        cout << entry.first << ": " << entry.second << " ";
    }
}

bool isAnagram(string s, string t)
{
    if (s.length() != t.length())
        return false;

    map<char, int> map1;
    for (int i = 0; i < s.length(); i++)
    {
        if (map1.count(s[i]))
        {
            map1[s[i]]++;
        }
        else
        {
            map1[s[i]] = 1;
        }
    }
    // printMap(map1);
    // cout << endl;

    for (int i = 0; i < t.length(); i++)
    {
        if (!map1.count(t[i]))
            return false;
        else if (map1[t[i]] == 0)
            return false;
        else
            map1[t[i]]--;
    }
    return true;
}

int main()
{
    string s1, s2;
    s1 = "rat";
    s2 = "tar";
    // s1 = "rat";
    // s2 = "car";
    // s1 = "anagram";
    // s2 = "nagaram";
    cout << isAnagram(s1, s2) << endl;
    return 0;
}